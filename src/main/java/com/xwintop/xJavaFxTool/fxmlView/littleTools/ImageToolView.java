package com.xwintop.xJavaFxTool.fxmlView.littleTools;

import org.springframework.context.annotation.Scope;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@Scope("prototype")
@FXMLView
public class ImageToolView extends AbstractFxmlView {

}